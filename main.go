package main

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"

	"go.etcd.io/bbolt"
	"github.com/gorilla/mux"
)

var (
	db *bbolt.DB

	secretsBucketName  = []byte("secrets")
	versionsBucketName = []byte("versions")

	errIncorrectKey     = errors.New("incorrect secret key")
	errParseSeed        = errors.New("could not parse seed")
	errRevisionNotFound = errors.New("revision not found")
)

type requestHandler func(vars map[string]string, out map[string]interface{}) error
type revision uint64

func (r revision) Bytes() (data []byte) {
	data = make([]byte, 8)
	binary.LittleEndian.PutUint64(data, uint64(r))
	return
}

func toRevision(data []byte) revision {
	return revision(binary.LittleEndian.Uint64(data))
}

func dbLookup(project, version, hash string) (rev revision, err error) {
	projectHashKey := []byte(project + ":" + version + ":" + hash)

	err = db.View(func(tx *bbolt.Tx) error {
		b := tx.Bucket(versionsBucketName)
		if b == nil {
			return nil
		}

		v := b.Get(projectHashKey)
		if v != nil {
			rev = toRevision(v)
		}

		return nil
	})

	if rev == 0 {
		err = errRevisionNotFound
	}
	if err != nil {
		return
	}

	return
}

func dbNext(project, version, hash, secret string, seed revision) (rev revision, err error) {
	projectLatestKey := []byte(project + ":" + version + ":latest")
	projectHashKey := []byte(project + ":" + version + ":" + hash)
	projectKey := []byte(project)
	secretVal := []byte(secret)

	err = db.Update(func(tx *bbolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists(secretsBucketName)
		if err != nil {
			return err
		}

		v := b.Get(projectKey)
		if v == nil {
			// Creating a new secret for this project
			b.Put(projectKey, secretVal)
		} else if !bytes.Equal(secretVal, v) {
			return errIncorrectKey
		}

		b, err = tx.CreateBucketIfNotExists(versionsBucketName)
		if err != nil {
			return err
		}

		v = b.Get(projectHashKey)
		if v != nil {
			// Found there's already a version for this hash key.
			rev = toRevision(v)
			return nil
		}

		v = b.Get(projectLatestKey)
		if v != nil {
			rev = toRevision(v)
		}

		if seed > rev {
			rev = seed
		}
		rev++

		b.Put(projectLatestKey, rev.Bytes())
		b.Put(projectHashKey, rev.Bytes())
		return nil
	})

	return
}

func lookupRevFunc(vars map[string]string, out map[string]interface{}) (err error) {
	project := vars["project"]
	version := vars["version"]
	hash := vars["hash"]

	out["project"] = project
	out["version"] = version
	out["hash"] = hash

	var rev revision
	rev, err = dbLookup(project, version, hash)
	if err != nil {
		return
	}

	if rev > 0 {
		out["ok"] = true
	}
	out["revision"] = rev
	return
}

func lookupOrNewRevFunc(vars map[string]string, out map[string]interface{}) (err error) {
	project := vars["project"]
	version := vars["version"]
	hash := vars["hash"]
	secret := vars["secret"]

	out["project"] = project
	out["version"] = version
	out["hash"] = hash

	var seed revision
	if v, e := strconv.ParseUint(vars["seed"], 10, 64); e == nil {
		seed = revision(v)
		out["seed"] = seed
	} else {
		return errParseSeed
	}

	var rev revision
	rev, err = dbLookup(project, version, hash)
	if err != nil && err != errRevisionNotFound {
		return
	}

	if rev > 0 {
		out["ok"] = true
		out["new"] = false
		out["revision"] = rev
		return
	}

	rev, err = dbNext(project, version, hash, secret, seed)
	if err != nil {
		return
	}

	if rev > 0 {
		out["ok"] = true
		out["new"] = true
		out["revision"] = rev
	}
	return
}

func handle(fn requestHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		w.Header().Add("Content-Type", "application/json")

		vars := mux.Vars(r)
		out := map[string]interface{}{
			"ok": false,
		}

		err := fn(vars, out)

		if err != nil {
			w.WriteHeader(599)
			out["err"] = err.Error()
		}

		data, err := json.Marshal(out)
		if err != nil {
			log.Println("error encoding output:", err.Error())
		} else {
			w.Write(data)
		}
	}
}

func main() {
	var err error

	db, err = bbolt.Open("my.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	r := mux.NewRouter()
	r.HandleFunc(`/rev/{project}/{version}/{hash:(?:[0-9a-f]+|latest)}`, handle(lookupRevFunc))
	r.HandleFunc(`/next/{project}/{version}/{secret:[0-9a-f]{16,32}}/{hash:[0-9a-f]+}/{seed:[0-9]*}`, handle(lookupOrNewRevFunc))
	r.PathPrefix(`/`).Handler(http.FileServer(http.Dir("./static/")))

	log.Println("Starting up server on :9081")
	log.Fatal(http.ListenAndServe(":9081", r))
}
