module gitlab.com/norganna-wow/misc/versionator

go 1.21.1

require (
	github.com/gorilla/mux v1.8.0
	go.etcd.io/bbolt v1.3.7
)

require golang.org/x/sys v0.4.0 // indirect
