{
  "swagger": "2.0",
  "info": {
    "description": "This is a simple API to manage revision numbers for projects",
    "version": "1.0.0",
    "title": "Versionator API",
    "contact": {
      "email": "ken@norganna.com"
    },
    "license": {
      "name": "MIT License"
    }
  },
  "host": "rev.norganna.com",
  "basePath": "/",
  "tags": [
    {
      "name": "rev",
      "description": "Fetch a revision number for a given project/version/hash."
    },
    {
      "name": "next",
      "description": "Fetch or create a new revision number for a given project/version/hash."
    }
  ],
  "schemes": [
    "https",
    "http"
  ],
  "paths": {
    "/rev/{project}/{version}/{hash}": {
      "get": {
        "tags": [
          "rev"
        ],
        "summary": "",
        "description": "",
        "operationId": "getRevision",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "project",
            "in": "path",
            "description": "A globally unique project identifier. First in, first served.\n\nRecommended format is: `com.organisation.project` to minimise conflicts with other teams/projects.",
            "required": true,
            "type": "string"
          },
          {
            "name": "version",
            "in": "path",
            "description": "Each version within a project has it's own revision numbers.\n\nIf you want unique revision numbers across your entrie project, leave blank.",
            "required": false,
            "type": "string"
          },
          {
            "name": "hash",
            "in": "path",
            "description": "The SHA (or other) hexadecimal hash string that uniquely identifies this revision.\n\nThe special string `latest` may be supplied to retrieve the current latest revision.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          },
          "599": {
            "description": "Error retrieving revision"
          }
        }
      }
    },
    "/next/{project}/{version}/{secret}/{hash}/{seed}": {
      "get": {
        "tags": [
          "next"
        ],
        "summary": "",
        "description": "",
        "operationId": "getNextRevision",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "project",
            "in": "path",
            "description": "A globally unique project identifier. First in, first served.\n\nRecommended format is: `com.organisation.project` to minimise conflicts with other teams/projects.",
            "required": true,
            "type": "string",
            "example": "org.acme.test-project"
          },
          {
            "name": "version",
            "in": "path",
            "description": "Each version within a project has it's own revision numbers.\n\nIf you want unique revision numbers across your entrie project, leave blank.",
            "required": false,
            "type": "string",
            "example": "1.0.0-beta"
          },
          {
            "name": "secret",
            "in": "path",
            "description": "A 16-32 character hexadecimal string that was supplied when first creating a project.\n\nIf this is your first call with a previously unused project identifier, you are creating the project and what you specify here will stick.",
            "required": true,
            "type": "string",
            "example": "1234567890abcdef"
          },
          {
            "name": "hash",
            "in": "path",
            "description": "The SHA (or other) hexadecimal hash string that uniquely identifies this revision.",
            "required": true,
            "type": "string",
            "example": "2daaecf08829480d38921318047a46629063ebc205bbd98f19de6b19a3ce406b"
          },
          {
            "name": "seed",
            "in": "path",
            "description": "A seed version.\n\nThe generated revision number will not be less than this supplied number. Useful to initialise the first revision or keep numbers in sync with external revision number generation.\n\nIf you don't require this feature, leave blank.",
            "required": false,
            "type": "string",
            "example": "1500"
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          },
          "599": {
            "description": "Error retrieving revision"
          }
        }
      }
    }
  }
}